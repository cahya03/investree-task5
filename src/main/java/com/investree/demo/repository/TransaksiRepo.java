package com.investree.demo.repository;

import com.investree.demo.model.Transaksi;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransaksiRepo extends PagingAndSortingRepository<Transaksi, Long> {

    @Query("select t from Transaksi t")
    public List<Transaksi> getList();

    @Query("select t from Transaksi t where t.id = :id")
    public Transaksi getById(@Param("id") Long id);
}
