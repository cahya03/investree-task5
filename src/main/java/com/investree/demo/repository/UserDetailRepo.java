package com.investree.demo.repository;


import com.investree.demo.model.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailRepo extends JpaRepository<UserDetail, Long> {

}
