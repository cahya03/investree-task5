package com.investree.demo.repository;

import com.investree.demo.model.PaymentHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PaymentHistoryRepo extends PagingAndSortingRepository<PaymentHistory, Long> {

    @Query("select ph from PaymentHistory ph")
    public List<PaymentHistory> getList();

    @Query("select ph from PaymentHistory ph where ph.id = :id")
    public PaymentHistory getById(@Param("id") Long id);
}
