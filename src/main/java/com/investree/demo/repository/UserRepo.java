package com.investree.demo.repository;

import com.investree.demo.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepo extends PagingAndSortingRepository<User, Long> {

    @Query("select u from User u")
    public List<User> getList();

    @Query("select u.id, u.username, u.isActive from User u where u.id = :id")
    public User getById(@Param("id") Long id);


}
