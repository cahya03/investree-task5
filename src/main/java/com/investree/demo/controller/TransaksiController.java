package com.investree.demo.controller;


import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepo;
import com.investree.demo.view.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/v1/transaksi")
public class TransaksiController {

    @Autowired
    public TransaksiRepo transaksiRepo;

    @Autowired
    TransaksiService transaksiService;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@RequestBody Transaksi objModel){
        Map obj = transaksiService.save(objModel);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
}
